package es.maincode.everis2.models

class TeamResponseVO (
    var status: Int,
    var message: String,
    var results: Int,
    var filters: List<String>,
    var teams: List<TeamVO>
)