package es.maincode.everis2.models

class TeamVO (
    var city: String? = null,
    var fullName: String? = null,
    var teamId: String? = null,
    var nickname: String? = null,
    var logo: String? = null,
    var shortName: String? = null,
    var nbaFranchise: String? = null,
    var leagues: LeaguesVO? = null
)