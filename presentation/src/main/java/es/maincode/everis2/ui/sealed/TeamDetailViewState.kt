package es.maincode.everis2.ui.sealed

import es.maincode.everis2.models.TeamVO

sealed class TeamDetailViewState {
    object Loading : TeamDetailViewState()
//    class ShowList(val movies: MovieResponseVO): TeamDetailViewState()
//    class Navegation (val movie: MovieVO) : TeamDetailViewState()
    class ShowDetail(val teamVO: TeamVO) : TeamDetailViewState()
    class ShowError(val any: Any): TeamDetailViewState()
}