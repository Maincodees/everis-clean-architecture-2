package es.maincode.everis2.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import es.maincode.domain.models.TeamBO
import es.maincode.domain.usecases.GetTeamDetail
import es.maincode.everis2.mappers.TeamVOMapper
import es.maincode.everis2.ui.sealed.TeamDetailViewState
import kotlinx.coroutines.launch

class TeamDetailViewModel(private val getTeamDetail: GetTeamDetail,
                          private val mapper: TeamVOMapper) : ViewModel() {

    private val _state = MutableLiveData<TeamDetailViewState>()
    private val teamId: Int? = null

    val stateTeamDetail: LiveData<TeamDetailViewState>
        get() {
            if (_state.value == null &&
                    teamId != null) refresh(teamId)
            return _state
        }


    private fun refresh(teamId: Int) {
        viewModelScope.launch {
            _state.value = TeamDetailViewState.Loading
//            getTeamDetail.invoke(teamId).fold(::ShowError, ::RenderDetail)
        }
    }

    private fun RenderDetail(teamBO: TeamBO) {
        _state.value = TeamDetailViewState.ShowDetail(mapper.transform(teamBO))
    }

//    private fun ShowDetail(movieResponseBO: MovieResponseBO) {
//        _state.value = TeamDetailViewState.ShowDetail(mapper.transform(movieResponseBO))
//    }

    private fun ShowError(any: Any) {
        _state.value = TeamDetailViewState.ShowError(any)
    }
}