package es.maincode.everis2.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import es.maincode.everis2.R
import es.maincode.everis2.ui.sealed.TeamDetailViewState
import es.maincode.everis2.ui.viewmodels.TeamDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class TeamDetailFragment() : Fragment() {

    private var teamId: Int? = null
    private val viewModel  by viewModel <TeamDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_team_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserve()
    }

    private fun initObserve() {
        viewModel.stateTeamDetail.observe(this, Observer (::renderDetail))
    }

    private fun renderDetail(stateTeamDetail: TeamDetailViewState?) {
        when (stateTeamDetail) {
            is TeamDetailViewState.Loading -> {

            }
            is TeamDetailViewState.ShowDetail -> {
                //render detail
                stateTeamDetail.teamVO
            }
        }
    }
}