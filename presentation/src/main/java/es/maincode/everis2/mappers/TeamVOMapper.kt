package es.maincode.everis2.mappers

import es.maincode.domain.models.LeagueBO
import es.maincode.domain.models.StandardBO
import es.maincode.domain.models.TeamBO
import es.maincode.everis2.models.LeaguesVO
import es.maincode.everis2.models.StandardVO
import es.maincode.everis2.models.TeamVO

class TeamVOMapper {
    fun transform(teamBO: TeamBO): TeamVO {
        val teamVO = TeamVO()
        return teamVO.apply {
            city = teamBO.city
            fullName = teamBO.fullName
            leagues = transform(teamBO.leagues)
            logo = teamBO.logo
            nbaFranchise = teamBO.nbaFranchise
            nickname = teamBO.nickname
            shortName = teamBO.shortName
            teamId = teamBO.teamId
        }
    }

    fun transform(leagueBO: LeagueBO?): LeaguesVO {
        val leaguesVO = LeaguesVO()
        return leaguesVO.apply {
            standard = transform(leagueBO?.standard)
        }
    }

    fun transform(standardBO: StandardBO?): StandardVO {
        val standardVO = StandardVO()
        return standardVO.apply {
            confName = standardBO?.confName
            divName = standardBO?.divName
        }
    }

    fun transform(listBO: List<TeamBO>): List<TeamVO> {
        return listBO.map { transform(it) }
    }
}