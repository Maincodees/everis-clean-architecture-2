package es.maincode.domain.repositories

import es.maincode.commonjava.Either
import es.maincode.domain.errors.TeamError
import es.maincode.domain.models.TeamBO
import es.maincode.domain.models.TeamResponseBO

interface TeamRepository {
    suspend fun getListTeams(error: Boolean): Either<TeamError, TeamResponseBO>
    suspend fun getTeamDetail(id: Int): Either<Any, TeamBO>
}