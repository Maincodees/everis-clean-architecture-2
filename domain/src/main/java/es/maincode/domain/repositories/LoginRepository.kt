package es.maincode.domain.repositories

import es.maincode.commonjava.Either
import es.maincode.domain.models.LoginBO

interface LoginRepository {
    suspend fun getLogin(): Either<Any, LoginBO>
}