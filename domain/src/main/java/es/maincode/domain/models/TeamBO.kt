package es.maincode.domain.models

class TeamBO (
    var city: String? =  null,
    var fullName: String? = null,
    var teamId: String? = null,
    var nickname: String? = null,
    var logo: String? = null,
    var shortName: String? = null,
    var allStar: String? = null,
    var nbaFranchise: String? = null,
    var leagues: LeagueBO? = null
)