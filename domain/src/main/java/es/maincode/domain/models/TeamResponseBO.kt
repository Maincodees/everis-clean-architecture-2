package es.maincode.domain.models

class TeamResponseBO (
    var status: Int? = null,
    var message: String? = null,
    var results: Int? = null,
    var filters: List<String>? = null,
    var teams: List<TeamBO>? = null
)