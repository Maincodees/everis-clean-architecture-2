package es.maincode.domain.usecases

import es.maincode.commonjava.Either
import es.maincode.domain.errors.TeamError
import es.maincode.domain.models.TeamResponseBO
import es.maincode.domain.repositories.TeamRepository

class GetTeams(private val repository: TeamRepository) {
    suspend fun invoke(error: Boolean): Either<TeamError, TeamResponseBO> = repository.getListTeams(error)
}
