package es.maincode.domain.usecases

import es.maincode.commonjava.Either
import es.maincode.domain.models.TeamBO
import es.maincode.domain.repositories.TeamRepository

class GetTeamDetail (private val teamRepository: TeamRepository) {
    suspend fun invoke(id: Int): Either<Any, TeamBO> = teamRepository.getTeamDetail(id)
}