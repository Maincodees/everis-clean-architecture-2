package es.maincode.domain.useCases

import es.maincode.commonjava.Either
import es.maincode.domain.repositories.LoginRepository
import es.maincode.domain.models.LoginBO

class GetLogin(private val repository: LoginRepository) {
    suspend fun  invoke(): Either<Any, LoginBO> = repository.getLogin()
}