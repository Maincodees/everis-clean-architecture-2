package es.maincode.data.mappers

import es.maincode.data.entities.LoginResponseEntity
import es.maincode.domain.models.LoginBO

class LoginMapper {

    fun transform (teamResponseEntity: LoginResponseEntity?): LoginBO {
        val loginBO = LoginBO()
        return loginBO.apply {
            status = teamResponseEntity?.status
        }
    }

}