package es.maincode.data.mappers

import es.maincode.data.entities.LeaguesEntity
import es.maincode.data.entities.StandardEntity
import es.maincode.data.entities.TeamEntity
import es.maincode.data.entities.TeamResponseEntity
import es.maincode.domain.models.LeagueBO
import es.maincode.domain.models.StandardBO
import es.maincode.domain.models.TeamBO
import es.maincode.domain.models.TeamResponseBO

class TeamMapper {
    fun transform (teamResponseEntity: TeamResponseEntity?): TeamResponseBO {
        val teamResponseBO = TeamResponseBO()
        return teamResponseBO.apply {
            message = teamResponseEntity?.message
            filters = teamResponseEntity?.filters
            results = teamResponseEntity?.results
            status = teamResponseEntity?.status
            teams = transform(teamResponseEntity?.teams)
        }
    }

    fun transform(listTeamEntity: List<TeamEntity>?) : List<TeamBO>? {
        return listTeamEntity?.map { transform(it) }
    }

    fun transform (teamEntity: TeamEntity?) : TeamBO {
        val teamBO = TeamBO()
        return teamBO.apply {
            allStar = teamEntity?.allStar
            city = teamEntity?.city
            fullName = teamEntity?.fullName
            leagues = transform(teamEntity?.leagues)
            logo = teamEntity?.logo
            nbaFranchise = teamEntity?.nbaFranchise
            nickname = teamEntity?.nickName
            shortName = teamEntity?.shortName
            teamId = teamEntity?.teamId
        }
    }

    fun transform(leaguesEntity: LeaguesEntity?) : LeagueBO {
        val leagueBO = LeagueBO()
        return leagueBO.apply {
            standard = transform(leaguesEntity?.standard)
        }
    }

    fun transform(standardEntity: StandardEntity?) : StandardBO {
        val standardBO = StandardBO()
        return standardBO.apply {
            confName = standardEntity?.confName
            divName = standardEntity?.divName
        }
    }
}