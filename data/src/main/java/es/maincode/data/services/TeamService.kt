package es.maincode.data.services

import es.maincode.data.entities.TeamResponseEntity
import retrofit2.Response
import retrofit2.http.GET

interface TeamService {

    fun getTeamDetail(id: Int): Response<TeamResponseEntity>

    @GET("teams")
    suspend fun getlistTeamsAsync(): Response<TeamResponseEntity>
}