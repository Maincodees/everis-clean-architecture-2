package es.maincode.data.services

import es.maincode.data.entities.LoginResponseEntity
import retrofit2.Response
import retrofit2.http.GET

interface LoginService {

    @GET("/my/api/login")
    suspend fun getLogin(): Response<LoginResponseEntity>
}