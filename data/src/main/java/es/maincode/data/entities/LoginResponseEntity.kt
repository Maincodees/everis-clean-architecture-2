package es.maincode.data.entities

data class LoginResponseEntity (
    var status: String? = null
)