package es.maincode.data.entities

data class TeamResponseEntity (
    var status: Int? = null,
    var message: String? = null,
    var results: Int? = null,
    var filters: List<String>? = null,
    var teams: List<TeamEntity>? = null
)