package es.maincode.data.repositories

import es.maincode.commonjava.Either
import es.maincode.data.mappers.LoginMapper
import es.maincode.data.services.LoginServiceManager
import es.maincode.domain.repositories.LoginRepository
import es.maincode.domain.models.LoginBO

class LoginRepositoryImpl(val mapper: LoginMapper) : LoginRepository {

    override suspend fun getLogin(): Either<Any, LoginBO> {
        return LoginServiceManager.service.getLogin().let {
            if (it.isSuccessful) Either.Right(mapper.transform(it.body())) else Either.Left("ERROR")
        }
    }
}