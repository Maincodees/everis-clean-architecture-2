package es.maincode.data.repositories


import es.maincode.commonjava.Either
import es.maincode.data.TeamServiceManager
import es.maincode.data.mappers.TeamMapper
import es.maincode.domain.errors.TeamError
import es.maincode.domain.models.TeamBO
import es.maincode.domain.models.TeamResponseBO
import es.maincode.domain.repositories.TeamRepository

class TeamRepositoryImpl (private val mapper: TeamMapper): TeamRepository {

    override suspend fun getListTeams(error: Boolean): Either<TeamError, TeamResponseBO> {
        return TeamServiceManager.service.getlistTeamsAsync().let {
            if (it.isSuccessful) Either.Right(mapper.transform(it.body())) else Either.Left(
                TeamError("ERROR")
            )
        }
    }

    override suspend fun getTeamDetail(id: Int): Either<Any, TeamBO> {
        return Either.Right(TeamBO())
    }
}